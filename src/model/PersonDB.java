package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PersonDB {

    private IntegerProperty id;
    private StringProperty firstName;
    private StringProperty lname;
    private IntegerProperty age;
    private StringProperty city;

    public PersonDB() {
        this.id = new SimpleIntegerProperty();
        this.firstName = new SimpleStringProperty();
        this.lname = new SimpleStringProperty();
        this.age = new SimpleIntegerProperty();
        this.city = new SimpleStringProperty();
    }


    private int id_;
    private String firstName_;
    private String lname_;
    private int age_;
    private String city_;

    public PersonDB(int id_, String firstName_, String lname_, int age_, String city_) {
        this.id_ = id_;
        this.firstName_ = firstName_;
        this.lname_ = lname_;
        this.age_ = age_;
        this.city_ = city_;
    }



    //id
    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty personIdProperty(){
        return id;
    }

    //first_name
    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    //last_name
    public String getLname() {
        return lname.get();
    }

    public void setLname(String lname) {
        this.lname.set(lname);
    }

    public StringProperty lastNameProperty() {
        return lname;
    }

    //age
    public int getAge() {
        return age.get();
    }

    public void setAge(int age) {
        this.age.set(age);
    }

    public IntegerProperty ageProperty(){
        return age;
    }

    //city
    public String getCity() {
        return city.get();
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public StringProperty cityProperty() {
        return city;
    }

    @Override
    public String toString() {
        return "Persons{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lname='" + lname + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }

}
