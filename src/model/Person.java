package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Person {

    private int id;
    private String firstName;
    private String lname;
    private int age;
    private String city;

    public Person(int id, String firstName, String lname, int age, String city) {
        this.id = id;
        this.firstName = firstName;
        this.lname = lname;
        this.age = age;
        this.city = city;
    }

    public Person() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Persons{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lname='" + lname + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }

    public Person copy(Person persons) {

        Person newPersons = new Person(persons.id, persons.firstName, persons.lname, persons.age, persons.city);

        return newPersons;
    }
}
