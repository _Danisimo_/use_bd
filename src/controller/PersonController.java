package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.PersonDB;
import util.CatalogDB;
import util.localDB.ServiceJSON;
import util.remoteDB.*;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class PersonController {

    @FXML
    private TextField idText;
    @FXML
    private TextField firstNameText;
    @FXML
    private TextField lNameText;
    @FXML
    private TextField ageText;
    @FXML
    private TextField cityText;
    @FXML
    private TextArea resultArea;
    @FXML
    private TableView personsTable;
    @FXML
    ComboBox<CatalogDB> catalogDB;
    @FXML
    private TableColumn<PersonDB, Integer> idColumn;
    @FXML
    private TableColumn<PersonDB, String> firstNameColumn;
    @FXML
    private TableColumn<PersonDB, String> lNameColumn;
    @FXML
    private TableColumn<PersonDB, Integer> ageColumn;
    @FXML
    private TableColumn<PersonDB, String> cityColumn;

    private Executor exec;
    private String choosenDB="";

    @FXML
    private void initialize() {
        catalogDB.getItems().setAll(CatalogDB.values());
        String choosenDB;

        exec = Executors.newCachedThreadPool((runnable) -> {
            Thread t = new Thread(runnable);
            t.setDaemon(true);
            return t;
        });

        idColumn.setCellValueFactory(cellData -> cellData.getValue().personIdProperty().asObject());
        firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        lNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        ageColumn.setCellValueFactory(cellData -> cellData.getValue().ageProperty().asObject());
        cityColumn.setCellValueFactory(cellData -> cellData.getValue().cityProperty());
    }

    @FXML
    private void createPerson(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {

        if (idText.getText().isEmpty()
                || firstNameText.getText().isEmpty()
                || lNameText.getText().isEmpty()
                || cityText.getText().isEmpty()
                || choosenDB.isEmpty()) {
            setInfoToTextArea("Please, fill in all fields!\nCheck that DataBase choosen.");
        } else {
            switch (choosenDB) {

                case "H2":
//                    try {
//                        H2.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Person created!: "
//                                + firstNameText.getText() + " "
//                                + lNameText.getText());
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while creating " + e);
//                        throw e;
//                    }

                    break;

                case "MySQL":
                    try {
                        MySQL.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
                        resultArea.setText("Person created!: "
                                + firstNameText.getText() + " "
                                + lNameText.getText());
                    } catch (SQLException e) {
                        resultArea.setText("Problem occurred while creating " + e);
                        throw e;
                    }

                    break;


                case "MySQL_Hibernate":
//                    try {
//                        MySQL_Hibernate.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Person created!: "
//                                + firstNameText.getText() + " "
//                                + lNameText.getText());
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while creating " + e);
//                        throw e;
//                    }

                    break;

                case "PostgreSQL":
                    try {
                        PostgreDB.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
                        resultArea.setText("Person created!: "
                                + firstNameText.getText() + " "
                                + lNameText.getText());
                    } catch (SQLException e) {
                        resultArea.setText("Problem occurred while creating " + e);
                        throw e;
                    }

                    break;

                case "Cassandra":
//                    try {
//                        Cassandra.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Person created!: "
//                                + firstNameText.getText() + " "
//                                + lNameText.getText());
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while creating " + e);
//                        throw e;
//                    }

                    break;

                case "Mongo":
//                    try {
//                        Mongo.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Person created!: "
//                                + firstNameText.getText() + " "
//                                + lNameText.getText());
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while creating " + e);
//                        throw e;
//                    }

                    break;

                case "HyperGraph":
//                    try {
//                        HyperGraph.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Person created!: "
//                                + firstNameText.getText() + " "
//                                + lNameText.getText());
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while creating " + e);
//                        throw e;
//                    }

                    break;



                case "JSON":
//                    try {
//                        JSON.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Person created!: "
//                                + firstNameText.getText() + " "
//                                + lNameText.getText());
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while creating " + e);
//                        throw e;
//                    }

                    break;

                case "CSV":
//                    try {
//                        CSV.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Person created!: "
//                                + firstNameText.getText() + " "
//                                + lNameText.getText());
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while creating " + e);
//                        throw e;
//                    }

                    break;

                case "XML":
//                    try {
//                        CSV.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Person created!: "
//                                + firstNameText.getText() + " "
//                                + lNameText.getText());
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while creating " + e);
//                        throw e;
//                    }

                    break;

                case "YAML":
//                    try {
//                        CSV.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Person created!: "
//                                + firstNameText.getText() + " "
//                                + lNameText.getText());
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while creating " + e);
//                        throw e;
//                    }

                    break;

                case "BINARY":
//                    try {
//                        CSV.create(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Person created!: "
//                                + firstNameText.getText() + " "
//                                + lNameText.getText());
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while creating " + e);
//                        throw e;
//                    }
                    break;
            }
        }
        reFresh();
        clearTextFieldsAfterOperation();
    }

    @FXML
    private void fillEmployeeTable(ActionEvent event) throws SQLException, ClassNotFoundException {
        reFresh();
        clearTextFieldsAfterOperation();
    }

    @FXML
    private void updatePerson(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {

        if (firstNameText.getText().isEmpty()
                && lNameText.getText().isEmpty()
                && cityText.getText().isEmpty()) {
            setInfoToTextArea("Please, fill in all fields!");
        } else {
            switch (choosenDB) {

                case "H2":
//                    try {
//                        H2.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//
                    break;
                case "MySQL":
                    try {
                        MySQL.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
                    } catch (SQLException e) {
                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
                    }

                    break;
                case "MySQL_Hibernate":
//                    try {
//                        MySQL_Hibernate.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//
                    break;
                case "PostgreSQL":
                    try {
                        PostgreDB.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
                    } catch (SQLException e) {
                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
                    }

                    break;
                case "Cassandra":
//                    try {
//                        CassandraDB.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//
                    break;
                case "Mongo":
//                    try {
//                        Mongo.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//                    clearTextFieldsAfterOperation();
                    break;
                case "HyperGraph":
//                    try {
//                        HyperGraph.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//
                    break;
                case "Redis":
//                    try {
//                        RedisDB.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//
                    break;
                case "JSON":
//                    try {
//                        JSON.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//
                    break;
                case "CSV":
//                    try {
//                        CSV.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//
                    break;
                case "XML":
//                    try {
//                        XML.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//
                    break;
                case "YAML":
//                    try {
//                        YAML.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//
                    break;
                case "BINARY":
//                    try {
//                        BINARY.update(idText.getText(), firstNameText.getText(), lNameText.getText(), ageText.getText(), cityText.getText());
//                        resultArea.setText("Information has been updated for, person id: " + idText.getText() + "\n");
//                    } catch (SQLException e) {
//                        resultArea.setText("Problem occurred while updating: id not found\n  " + e);
//                    }
//
                    break;
            }
            reFresh();
            clearTextFieldsAfterOperation();
        }
    }

    @FXML
    private void deletePerson(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        switch (choosenDB){
            case "H2":
//                try {
//                    H2.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;
            case "MySQL":
                try {
                    MySQL.delete(idText.getText());
                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting person " + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
//                try {
//                    MySQL_Hibernate.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;
            case "PostgreSQL":
                try {
                    PostgreDB.delete(idText.getText());
                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting person " + e);
                    throw e;
                }
                break;
            case "Cassandra":
//                try {
//                    CassandraDB.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;
            case "Mongo":
//                try {
//                    Mongo.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;
            case "HyperGraph":
//                try {
//                    HyperGraph.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;

            case "Redis":
//                try {
//                    RedisDB.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;

            case "JSON":
//                try {
//                    JSON.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;
            case "CSV":
//                try {
//                    CSV.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;
            case "XML":
//                try {
//                    XML.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;
            case "YAML":
//                try {
//                    YAML.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;

            case "BINARY":
//                try {
//                    BINARY.delete(idText.getText());
//                    resultArea.setText("Person deleted!\nPerson id: " + idText.getText());
//                } catch (SQLException e) {
//                    resultArea.setText("Problem occurred while deleting person " + e);
//                    throw e;
//                }
                break;
        }
        reFresh();
        clearTextFieldsAfterOperation();
    }

    @FXML
    private void deleteAll(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        //добавить alert - типа ты в своем уме))
        switch (choosenDB){
            case "H2":
                break;
            case "MySQL":
                try {
                    MySQL.deleteAll();
                    resultArea.setText("All persons have been deleted!");
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting persons " + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
                break;
            case "PostgreSQL":
                try {
                    PostgreDB.deleteAll();
                    resultArea.setText("All persons have been deleted!");
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting persons " + e);
                    throw e;
                }
                break;
            case "Cassandra":
                break;
            case "Mongo":
                break;
            case "HyperGraph":
                break;
            case "Redis":
                break;
            case "JSON":
                break;
            case "CSV":
                break;
            case "XML":
                break;
            case "YAML":
                break;
            case "BINARY":
                break;
        }
        reFresh();
        clearTextFieldsAfterOperation();
    }

    @FXML
    private void deleteAllByFirstName(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        switch (choosenDB){
            case "H2":
                break;
            case "MySQL":
                try {
                    MySQL.deleteAllByFirstName(firstNameText.getText());
                    resultArea.setText("Deleted all persons with FirstName: " + firstNameText.getText());
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting persons " + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
                break;
            case "PostgreSQL":
                try {
                    PostgreDB.deleteAllByFirstName(firstNameText.getText());
                    resultArea.setText("Deleted all persons with FirstName: " + firstNameText.getText());
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting persons " + e);
                    throw e;
                }
                break;
            case "Cassandra":
                break;
            case "Mongo":
                break;
            case "HyperGraph":
                break;
            case "Redis":
                break;
            case "JSON":
                break;
            case "CSV":
                break;
            case "XML":
                break;
            case "YAML":
                break;
            case "BINARY":
                break;
        }
        reFresh();
        clearTextFieldsAfterOperation();
    }

    @FXML
    private void deleteAllByLastName(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        switch (choosenDB){
            case "H2":
                break;
            case "MySQL":
                try {
                    MySQL.deleteAllByLastName(lNameText.getText());
                    resultArea.setText("Deleted all persons with LastName: " + lNameText.getText());
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting persons " + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
                break;
            case "PostgreSQL":
                try {
                    PostgreDB.deleteAllByLastName(lNameText.getText());
                    resultArea.setText("Deleted all persons with LastName: " + lNameText.getText());
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting persons " + e);
                    throw e;
                }
                break;
            case "Cassandra":
                break;
            case "Mongo":
                break;
            case "HyperGraph":
                break;
            case "Redis":
                break;
            case "JSON":
                break;
            case "CSV":
                break;
            case "XML":
                break;
            case "YAML":
                break;
            case "BINARY":
                break;
        }
        reFresh();
        clearTextFieldsAfterOperation();
    }

    @FXML
    private void deleteAllByAge(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        switch (choosenDB){
            case "H2":
                break;
            case "MySQL":
                try {
                    MySQL.deleteAllByAge(ageText.getText());
                    resultArea.setText("Deleted all persons with age: " + ageText.getText());
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting persons " + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
                break;
            case "PostgreSQL":
                try {
                    PostgreDB.deleteAllByAge(ageText.getText());
                    resultArea.setText("Deleted all persons with age: " + ageText.getText());
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting persons " + e);
                    throw e;
                }
                break;
            case "Cassandra":
                break;
            case "Mongo":
                break;
            case "HyperGraph":
                break;
            case "Redis":
                break;
            case "JSON":
                break;
            case "CSV":
                break;
            case "XML":
                break;
            case "YAML":
                break;
            case "BINARY":
                break;
        }
        reFresh();
        clearTextFieldsAfterOperation();
    }

    @FXML
    private void deleteAllByCity(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        switch (choosenDB){
            case "H2":
                break;
            case "MySQL":
                try {
                    MySQL.deleteAllByCity(cityText.getText());
                    resultArea.setText("Deleted all persons with city: " + cityText.getText());
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting persons " + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
                break;
            case "PostgreSQL":
                try {
                    PostgreDB.deleteAllByCity(cityText.getText());
                    resultArea.setText("Deleted all persons with city: " + cityText.getText());
                } catch (SQLException e) {
                    resultArea.setText("Problem occurred while deleting persons " + e);
                    throw e;
                }
                break;
            case "Cassandra":
                break;
            case "Mongo":
                break;
            case "HyperGraph":
                break;
            case "Redis":
                break;
            case "JSON":
                break;
            case "CSV":
                break;
            case "XML":
                break;
            case "YAML":
                break;
            case "BINARY":
                break;
        }
        reFresh();
        clearTextFieldsAfterOperation();
    }

    @FXML
    private void searchPersonByID(ActionEvent actionEvent) throws ClassNotFoundException, SQLException {
        switch (choosenDB){
            case "H2":
                break;
            case "MySQL":
                try {
                    //Get Person information
                    PersonDB person =MySQL.searchByID(idText.getText());
                    //Populate Person on TableView and Display on TextArea
                    populateAndShowPerson(person);

                } catch (SQLException e) {
                    e.printStackTrace();
                    resultArea.setText("Error occurred while getting employee information from DB.\n" + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
                break;
            case "PostgreSQL":
                try {
                    //Get Person information
                    PersonDB person =PostgreDB.searchByID(idText.getText());
                    //Populate Person on TableView and Display on TextArea
                    populateAndShowPerson(person);

                } catch (SQLException e) {
                    e.printStackTrace();
                    resultArea.setText("Error occurred while getting employee information from DB.\n" + e);
                    throw e;
                }
                break;
            case "Cassandra":
                break;
            case "Mongo":
                break;
            case "HyperGraph":
                break;
            case "Redis":
                break;
            case "JSON":
                break;
            case "CSV":
                break;
            case "XML":
                break;
            case "YAML":
                break;
            case "BINARY":
                break;
        }

    }

    @FXML
    private void searchAllPersonsByFirstName(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        switch (choosenDB){
            case "H2":
                break;
            case "MySQL":
                try {
                    //Get all Persons information
                    ObservableList<PersonDB> personsData = MySQL.searchByFirstName(firstNameText.getText());
                    //Populate Persons on TableView
                    populatePersons(personsData);

                    if (personsData.isEmpty()) {
                        setInfoToTextArea("Persons not found!");
                    } else {
                        setInfoToTextArea("Persons found:" + personsData.size());
                    }
                } catch (SQLException e) {
                    System.out.println("Error occurred while getting employees information from DB.\n" + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
                break;
            case "PostgreSQL":
                try {
                    //Get all Persons information
                    ObservableList<PersonDB> personsData = PostgreDB.searchByFirstName(firstNameText.getText());
                    //Populate Persons on TableView
                    populatePersons(personsData);

                    if (personsData.isEmpty()) {
                        setInfoToTextArea("Persons not found!");
                    } else {
                        setInfoToTextArea("Persons found:" + personsData.size());
                    }
                } catch (SQLException e) {
                    System.out.println("Error occurred while getting employees information from DB.\n" + e);
                    throw e;
                }
                break;
            case "Cassandra":
                break;
            case "Mongo":
                break;
            case "HyperGraph":
                break;
            case "Redis":
                break;
            case "JSON":
                break;
            case "CSV":
                break;
            case "XML":
                break;
            case "YAML":
                break;
            case "BINARY":
                break;
        }

    }

    @FXML
    private void searchAllPersonsByLastName(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        switch (choosenDB){
            case "H2":
                break;
            case "MySQL":
                try {
                    //Get all Persons information
                    ObservableList<PersonDB> personsData = MySQL.searchAllByLastName(lNameText.getText());
                    //Populate Persons on TableView
                    populatePersons(personsData);

                    if (personsData.isEmpty()) {
                        setInfoToTextArea("Persons not found!");
                    } else {
                        setInfoToTextArea("Persons found:" + personsData.size());
                    }
                } catch (SQLException e) {
                    System.out.println("Error occurred while getting employees information from DB.\n" + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
                break;
            case "PostgreSQL":
                try {
                    //Get all Persons information
                    ObservableList<PersonDB> personsData = PostgreDB.searchAllByLastName(lNameText.getText());
                    //Populate Persons on TableView
                    populatePersons(personsData);

                    if (personsData.isEmpty()) {
                        setInfoToTextArea("Persons not found!");
                    } else {
                        setInfoToTextArea("Persons found:" + personsData.size());
                    }
                } catch (SQLException e) {
                    System.out.println("Error occurred while getting employees information from DB.\n" + e);
                    throw e;
                }
                break;
            case "Cassandra":
                break;
            case "Mongo":
                break;
            case "HyperGraph":
                break;
            case "Redis":
                break;
            case "JSON":
                break;
            case "CSV":
                break;
            case "XML":
                break;
            case "YAML":
                break;
            case "BINARY":
                break;
        }

    }

    @FXML
    private void searchAllPersonsByAge(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        switch (choosenDB){
            case "H2":
                break;
            case "MySQL":
                try {
                    //Get all Persons information
                    ObservableList<PersonDB> personsData = MySQL.searchAllByAge(ageText.getText());
                    //Populate Persons on TableView
                    populatePersons(personsData);

                    if (personsData.isEmpty()) {
                        setInfoToTextArea("Persons not found!");
                    } else {
                        setInfoToTextArea("Persons found:" + personsData.size());
                    }
                } catch (SQLException e) {
                    System.out.println("Error occurred while getting employees information from DB.\n" + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
                break;
            case "PostgreSQL":
                try {
                    //Get all Persons information
                    ObservableList<PersonDB> personsData = PostgreDB.searchAllByAge(ageText.getText());
                    //Populate Persons on TableView
                    populatePersons(personsData);

                    if (personsData.isEmpty()) {
                        setInfoToTextArea("Persons not found!");
                    } else {
                        setInfoToTextArea("Persons found:" + personsData.size());
                    }
                } catch (SQLException e) {
                    System.out.println("Error occurred while getting employees information from DB.\n" + e);
                    throw e;
                }
                break;
            case "Cassandra":
                break;
            case "Mongo":
                break;
            case "HyperGraph":
                break;
            case "Redis":
                break;
            case "JSON":
                break;
            case "CSV":
                break;
            case "XML":
                break;
            case "YAML":
                break;
            case "BINARY":
                break;
        }

    }

    @FXML
    private void searchAllPersonsByCity(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        switch (choosenDB){
            case "H2":
                break;
            case "MySQL":
                try {
                    //Get all Persons information
                    ObservableList<PersonDB> personsData = MySQL.searchAllByCity(cityText.getText());
                    //Populate Persons on TableView
                    populatePersons(personsData);

                    if (personsData.isEmpty()) {
                        setInfoToTextArea("Persons not found!");
                    } else {
                        setInfoToTextArea("Persons found:" + personsData.size());
                    }
                } catch (SQLException e) {
                    System.out.println("Error occurred while getting employees information from DB.\n" + e);
                    throw e;
                }
                break;
            case "MySQL_Hibernate":
                break;
            case "PostgreSQL":
                try {
                    //Get all Persons information
                    ObservableList<PersonDB> personsData = PostgreDB.searchAllByCity(cityText.getText());
                    //Populate Persons on TableView
                    populatePersons(personsData);

                    if (personsData.isEmpty()) {
                        setInfoToTextArea("Persons not found!");
                    } else {
                        setInfoToTextArea("Persons found:" + personsData.size());
                    }
                } catch (SQLException e) {
                    System.out.println("Error occurred while getting employees information from DB.\n" + e);
                    throw e;
                }
                break;
            case "Cassandra":
                break;
            case "Mongo":
                break;
            case "HyperGraph":
                break;
            case "Redis":
                break;
            case "JSON":
                break;
            case "CSV":
                break;
            case "XML":
                break;
            case "YAML":
                break;
            case "BINARY":
                break;
        }

    }

    @FXML
    private void populateAndShowPerson(PersonDB person) throws ClassNotFoundException {
        if (person != null) {
            populatePerson(person);
            setPersInfoToTextArea(person);
        } else {
            resultArea.setText("This person does not exist!\n");
        }
    }

    @FXML
    private void populatePerson(PersonDB person) throws ClassNotFoundException {
        ObservableList<PersonDB> personData = FXCollections.observableArrayList();
        personData.add(person);
        personsTable.setItems(personData);
    }

    @FXML
    private void populatePersons(ObservableList<PersonDB> personsData) throws ClassNotFoundException {
        //Set items to the employeeTable
        personsTable.setItems(personsData);
    }

    @FXML
    private void setPersInfoToTextArea(PersonDB person) {
        resultArea.setText("Person found:\n" + person.getLname() + " " + person.getFirstName());
    }

    @FXML
    private void setInfoToTextArea(String str) {
        resultArea.setText(str);
    }

    private void clearTextFieldsAfterOperation() {
        idText.setText("");
        firstNameText.setText("");
        lNameText.setText("");
        ageText.setText("");
        cityText.setText("");
    }

    private void reFresh(){
        choosenDB = catalogDB.getValue().toString();

        switch (choosenDB) {
            case "H2":
//                Task<List<PersonDB>> task = new Task<List<PersonDB>>() {
//                    @Override
//                    protected ObservableList<PersonDB> call() throws Exception {
//                        return H2.read();
//                    }
//                };
//
//                task.setOnFailed(e -> task.getException().printStackTrace());
//                task.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) task.getValue()));
//                exec.execute(task);
                break;
//        }
            case "MySQL":
                Task<List<PersonDB>> taskMySQL = new Task<List<PersonDB>>() {
                    @Override
                    protected ObservableList<PersonDB> call() throws Exception {
                        return MySQL.read();
                    }
                };
                taskMySQL.setOnFailed(e -> taskMySQL.getException().printStackTrace());
                taskMySQL.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskMySQL.getValue()));
                exec.execute(taskMySQL);
                break;

            case "MySQL_Hibernate":
//                Task<List<PersonDB>> taskMySQL_Hibernate = new Task<List<PersonDB>>() {
//                    @Override
//                    protected ObservableList<PersonDB> call() throws Exception {
//                        return MySQL_Hibernate.read();
//                    }
//                };
//                taskMySQL_Hibernate.setOnFailed(e -> taskMySQL_Hibernate.getException().printStackTrace());
//                taskMySQL_Hibernate.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskMySQL_Hibernate.getValue()));
//                exec.execute(taskMySQL_Hibernate);
                break;

            case "PostgreSQL":

                Task<List<PersonDB>> taskPostgreSQL = new Task<List<PersonDB>>() {
                    @Override
                    protected ObservableList<PersonDB> call() throws Exception {
                        return PostgreDB.read();
                    }
                };

                taskPostgreSQL.setOnFailed(e -> taskPostgreSQL.getException().printStackTrace());
                taskPostgreSQL.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskPostgreSQL.getValue()));
                exec.execute(taskPostgreSQL);
                break;

            case "Cassandra":
//                Task<List<PersonDB>> taskCassandra = new Task<List<PersonDB>>() {
//                    @Override
//                    protected ObservableList<PersonDB> call() throws Exception {
//                        return CassandraDB.read();
//                    }
//                };
//
//                taskCassandra.setOnFailed(e -> taskCassandra.getException().printStackTrace());
//                taskCassandra.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskCassandra.getValue()));
//                exec.execute(taskCassandra);
                break;

            case "Mongo":
//                Task<List<PersonDB>> taskMongo = new Task<List<PersonDB>>() {
//                    @Override
//                    protected ObservableList<PersonDB> call() throws Exception {
//                        return Mongo.read();
//                    }
//                };
//
//                taskMongo.setOnFailed(e -> taskMongo.getException().printStackTrace());
//                taskMongo.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskMongo.getValue()));
//                exec.execute(taskMongo);
                break;

            case "HyperGraph":
//                Task<List<PersonDB>> taskHyperGraph = new Task<List<PersonDB>>() {
//                    @Override
//                    protected ObservableList<PersonDB> call() throws Exception {
//                        return HyperGraph.read();
//                    }
//                };
//
//                taskHyperGraph.setOnFailed(e -> taskHyperGraph.getException().printStackTrace());
//                taskHyperGraph.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskHyperGraph.getValue()));
//                exec.execute(taskHyperGraph);
                break;



            case "JSON":
//                Task<List<PersonDB>> taskJSON = new Task<List<PersonDB>>() {
//                    @Override
//                    protected ObservableList<PersonDB> call() throws Exception {
//                        return ;
//                    }
//                };
//
//                taskJSON.setOnFailed(e -> taskJSON.getException().printStackTrace());
//                taskJSON.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskJSON.getValue()));
//                exec.execute(taskJSON);
                break;
            case "CSV":
//                Task<List<PersonDB>> taskCSV = new Task<List<PersonDB>>() {
//                    @Override
//                    protected ObservableList<PersonDB> call() throws Exception {
//                        return ;
//                    }
//                };
//
//                taskCSV.setOnFailed(e -> taskCSV.getException().printStackTrace());
//                taskCSV.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskCSV.getValue()));
//                exec.execute(taskCSV);
                break;
            case "XML":
//                Task<List<PersonDB>> taskXML = new Task<List<PersonDB>>() {
//                    @Override
//                    protected ObservableList<PersonDB> call() throws Exception {
//                        return ;
//                    }
//                };
//
//                taskXML.setOnFailed(e -> taskXML.getException().printStackTrace());
//                taskXML.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskXML.getValue()));
//                exec.execute(taskXML);
                break;
            case "YAML":
//                Task<List<PersonDB>> taskYAML = new Task<List<PersonDB>>() {
//                    @Override
//                    protected ObservableList<PersonDB> call() throws Exception {
//                        return ;
//                    }
//                };
//
//                taskYAML.setOnFailed(e -> taskYAML.getException().printStackTrace());
//                taskYAML.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskYAML.getValue()));
//                exec.execute(taskYAML);
                break;
            case "BINARY":
//                Task<List<PersonDB>> taskBINARY = new Task<List<PersonDB>>() {
//                    @Override
//                    protected ObservableList<PersonDB> call() throws Exception {
//                        return ;
//                    }
//                };
//
//                taskBINARY.setOnFailed(e -> taskBINARY.getException().printStackTrace());
//                taskBINARY.setOnSucceeded(e -> personsTable.setItems((ObservableList<PersonDB>) taskBINARY.getValue()));
//                exec.execute(taskBINARY);
                break;
        }

        clearTextFieldsAfterOperation();

    }
}






