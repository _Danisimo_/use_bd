package util.localDB;

import model.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServiceYAML {

    String yamlFile = "persons.yaml";

    private void writeYAML(Person persons) {

        String dataSave = dataSave();
        try {
            FileWriter writer = new FileWriter(yamlFile, true);
            List<Person> person = Arrays.asList(persons);
            for (Person p : person) {
                writer.write(personToConsole(p));
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String dataSave() {
        File file = new File("C:\\Users\\Artemiy\\ConsoleClientTask\\src\\test\\DataBase.yaml");
        String personDataSave = " ";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            {
                personDataSave = br.readLine();
            }
        } catch (IOException e) {
        }
        return personDataSave;
    }

    private String personToConsole(Person person) {
        String construct = "persona" + person.getId() + ":"
                + "\n\t - id : " + person.getId()
                + "\n\t - first name : " + person.getFirstName()
                + "\n\t - second name : " + person.getLname()
                + "\n\t - age : " + person.getAge()
                + "\n\t - city : " + person.getCity() + "\n  ";

        return construct;
    }

    public void yamlSaveAll(ArrayList<Person> persons) {
        for (int i = 0; i < persons.size(); i++)
            writeYAML(persons.get(i));
    }

    public ArrayList<Person> parserYAML(){

        ArrayList<Person> person =new ArrayList<>();
        String line ="";
        String yamlSplitBy = ": ";

        try (BufferedReader br = new BufferedReader(new FileReader(yamlFile))) {
            Person persons = new Person();
            while ((line = br.readLine()) != null) {
                String[]split = line.split(yamlSplitBy);

                if (line.indexOf("id") != -1) {
                    persons.setId(Integer.parseInt(split[1]));
                    continue;
                }
                if (line.indexOf("first") != -1){
                    persons.setFirstName(split[1]);
                    continue;
                }
                if (line.indexOf("second") != -1){
                    persons.setLname(split[1]);
                    continue;
                }
                if (line.indexOf("age") != -1){
                    persons.setAge(Integer.parseInt(split[1]));
                    continue;
                }
                if (line.indexOf("city") != -1) {
                    persons.setCity(split[1]);
                    Person newPersons = new Person();
                    newPersons = newPersons.copy(persons);
                    person.add(newPersons);

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return person;
    }

    public void deleteYAML() {
        try {
            FileWriter writer = new FileWriter(yamlFile);
            writer.write("persons:\n  ");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
