package util.localDB;

import model.Person;

import java.io.*;
import java.util.ArrayList;

public class ServiceBinaryFile {

    String bin ="person.bin";

    private void writeBin(Person persons){

        try(DataOutputStream dos = new DataOutputStream(new FileOutputStream(bin,true))) {
            dos.writeInt(persons.getId());
            dos.writeUTF(persons.getFirstName());
            dos.writeUTF(persons.getLname());
            dos.writeInt(persons.getAge());
            dos.writeUTF(persons.getCity());
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public ArrayList<Person> ParserBin(){

        ArrayList<Person> persons = new ArrayList<>();
        try(DataInputStream dos = new DataInputStream(new FileInputStream(bin))) {

            while (true) {
                persons.add(new Person(dos.readInt(),dos.readUTF(),dos.readUTF(),dos.readInt(),dos.readUTF()));
            }

        }
        catch(IOException e){
//            e.printStackTrace();
        }
        return persons;
    }

    public void deleteBin(){
        try {
            FileWriter writer = new FileWriter(bin);
            writer.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveAllBin(ArrayList<Person> persons) {
        for (int i = 0; i < persons.size(); i++)
            writeBin(persons.get(i));
    }

}
