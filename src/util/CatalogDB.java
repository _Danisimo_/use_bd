package util;

public enum CatalogDB {
    H2,
    MySQL,
    MySQL_Hibernate,
    PostgreSQL,
    Cassandra,
    Mongo,
    HyperGraph,
    Redis,
    JSON,
    CSV,
    XML,
    YAML,
    BINARY
}
