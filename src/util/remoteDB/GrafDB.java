package util.remoteDB;

import model.Person;
import model.PersonDAO;
import java.io.File;
import org.neo4j.driver.*;

//import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GrafDB implements PersonDAO {

//    TABLE Persons
//            (
//             id int PRIMARY KEY NOT NULL,
//             firstName varchar(45),
//             lname varchar(45),
//             age int,
//             city varchar(45)
//)

    //  Database credentials
    protected static final String DB_URL = System.getenv("bolt://hobby-paoogepljjohgbkegpllnkfl.dbs.graphenedb.com:24787");
    protected static final String USER = System.getenv("vadymhor");
    protected static final String PASS = System.getenv("b.4MS864zchaee.of6yGH3JR3ik7Er4");
    protected static Connection conn = null;
    //

    Config.ConfigBuilder builder = Config.builder().withEncryption();
    Config config = builder.build();

    Driver driver = GraphDatabase.driver( DB_URL, AuthTokens.basic( USER, PASS ), config );

    Session session = driver.session();

    Result result = session.run("MATCH (:Person {name: 'Tom Hanks'})-[:ACTED_IN]->(movies) RETURN movies.title AS title");

//    while ( result.hasNext() )
//    {
//        Record record = result.next();
//        System.out.println( record.get("title").asString() );
//    }
//
//    session.close();
//    driver.close();
    //
    //Пробросить все команды через интерфейс и переопределить в классах DB
//    @Override
//    public void create(Person p) throws SQLException { }

    @Override
    public void create() {

    }

    @Override
    public void update(Person p) throws SQLException { }

    @Override
    public void delete(Person p) { }
//
//    //CRUD
    public static void conect(){

//Подключаем драйвер postgre
//        try {
//            Class.forName("org.neo4j.Driver");
//        } catch (ClassNotFoundException e) {
//            System.out.println("GrafDB neo4j Driver is not found. Include it in your library path ");
//            e.printStackTrace();
//            return;
//        }

        Config.ConfigBuilder builder = Config.builder().withEncryption();
        Config config = builder.build();

        Driver driver = GraphDatabase.driver( DB_URL, AuthTokens.basic( USER, PASS ), config );

        Session session = driver.session();
        //Подключаемся к базе
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
            return;
        }
    }
//
//    public static void create() throws SQLException {
//
//        conect();
//        Statement stmt = null;
//
//        try {
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//
//            stmt.addBatch(
//                    "INSERT INTO Persons " +
//                            "VALUES(15, 'Himavari', 'Hiroku', 15, 'Kanoha')");
//
//            stmt.executeBatch();
//            conn.commit();
//
//        } catch (BatchUpdateException b) {
//
//        } catch (SQLException ex) {
//
//        } finally {
//            if (stmt != null) {
//                stmt.close();
//            }
//            conn.setAutoCommit(true);
//        }
//    }
//
//    public static List<Person> read() throws SQLException {
//        conect();
//        //Создаем объект для создания запросов (Statement)
//        Statement stmt = conn.createStatement();
//
//        //Создаем запрос и получаем результат запроса
//        ResultSet rs = stmt.executeQuery("Select * from Persons");
//
//        //Проходимся по елементам результата запроса
//        // Количество колонок в результирующем запросе
//        int columns = rs.getMetaData().getColumnCount();
//
//        List<Person> persons = new ArrayList();
//        // Перебор строк с данными
//        while (rs.next()) {
//            persons.add(new Person(rs.getInt("id"), rs.getString("firstName"), rs.getString("lName"), rs.getInt("age"), rs.getString("city")));
////            for (int i = 1; i <= columns; i++) {
////                System.out.print(rs.getString(i) + "\t");
////            }
////            System.out.println();
//            stmt.close();
//        }
////        System.out.println();
//        return persons;
//    }
//
//    public static void update() throws SQLException {
//
//        conect();
//        Statement stmt = null;
//
//        try {
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//
//            stmt.addBatch(
//                    //нужно перезаписать все текущие значения филдов
//                    "UPDATE Persons SET city='Mariupol' WHERE id>10");
//
//            stmt.executeBatch();
//            conn.commit();
//
//        } catch (BatchUpdateException b) {
//
//        } catch (SQLException ex) {
//
//        } finally {
//            if (stmt != null) {
//                stmt.close();
//            }
//            conn.setAutoCommit(true);
//        }
//    }
//
//    public static void delete(int id) throws SQLException{
//
//        conect();
//        Statement stmt = null;
//
//        try {
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//
//            stmt.addBatch(
//                    //'Наш филд id из филда формы'
//                    "DELETE FROM Persons Where id="+id);
//
//            stmt.executeBatch();
//            conn.commit();
//
//        } catch (BatchUpdateException b) {
//
//        } catch (SQLException ex) {
//
//        } finally {
//            if (stmt != null) {
//                stmt.close();
//            }
//            conn.setAutoCommit(true);
//        }
//
//    }
//
//    //Delete
//    public static void deleteAll() throws SQLException{
//
//        conect();
//        Statement stmt = null;
//
//        try {
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//
//            stmt.addBatch(
//                    //'Наш филд id из филда формы'
//                    "DELETE FROM Persons");
//
//            stmt.executeBatch();
//            conn.commit();
//
//        } catch (BatchUpdateException b) {
//
//        } catch (SQLException ex) {
//
//        } finally {
//            if (stmt != null) {
//                stmt.close();
//            }
//            conn.setAutoCommit(true);
//        }
//
//    }
//
//    public static void deleteAllByFirstName(String firstName) throws SQLException{
//
//        conect();
//        Statement stmt = null;
//
//        try {
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//
//            stmt.addBatch(
//                    //'Наш филд id из филда формы'
//                    "DELETE FROM Persons Where firstName="+firstName);
//
//            stmt.executeBatch();
//            conn.commit();
//
//        } catch (BatchUpdateException b) {
//
//        } catch (SQLException ex) {
//
//        } finally {
//            if (stmt != null) {
//                stmt.close();
//            }
//            conn.setAutoCommit(true);
//        }
//
//    }
//
//    public static void deleteAllByLastName(String lName) throws SQLException{
//
//        conect();
//        Statement stmt = null;
//
//        try {
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//
//            stmt.addBatch(
//                    //'Наш филд id из филда формы'
//                    "DELETE FROM Persons Where lName="+lName);
//
//            stmt.executeBatch();
//            conn.commit();
//
//        } catch (BatchUpdateException b) {
//
//        } catch (SQLException ex) {
//
//        } finally {
//            if (stmt != null) {
//                stmt.close();
//            }
//            conn.setAutoCommit(true);
//        }
//
//    }
//
//    public static void deleteAllByAge(int age) throws SQLException{
//
//        conect();
//        Statement stmt = null;
//
//        try {
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//
//            stmt.addBatch(
//                    //'Наш филд id из филда формы'
//                    "DELETE FROM Persons Where age="+age);
//
//            stmt.executeBatch();
//            conn.commit();
//
//        } catch (BatchUpdateException b) {
//
//        } catch (SQLException ex) {
//
//        } finally {
//            if (stmt != null) {
//                stmt.close();
//            }
//            conn.setAutoCommit(true);
//        }
//
//    }
//
//    public static void deleteAllByCity(String city) throws SQLException{
//
//        conect();
//        Statement stmt = null;
//
//        try {
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//
//            stmt.addBatch(
//                    //'Наш филд id из филда формы'
//                    "DELETE FROM Persons Where city="+city);
//
//            stmt.executeBatch();
//            conn.commit();
//
//        } catch (BatchUpdateException b) {
//
//        } catch (SQLException ex) {
//
//        } finally {
//            if (stmt != null) {
//                stmt.close();
//            }
//            conn.setAutoCommit(true);
//        }
//
//    }
//
//    //Search
//    public static Person searchByID(int id) throws SQLException {
//        conect();
//
//        //Создаем объект для создания запросов (Statement)
//        Statement stmt = conn.createStatement();
//
//        //Создаем запрос и получаем результат запроса
//        ResultSet rs = stmt.executeQuery("Select * from Persons where id="+id);
//
//        Person  person = null;
//        if(rs.next()) {
//            person = new Person(rs.getInt("id"), rs.getString("firstName"), rs.getString("lastName"), rs.getInt("age"), rs.getString("city"));
//            rs.close();
//            stmt.close();
//        } else {
//            //возможно нужно вернуть пустые значения
//            rs.close();
//            stmt.close();
//        }
//
//        return person;
//    }
//
//    public static List<Person> searchAllByFirstName(String firstName) throws SQLException{
//
//        conect();
//
//        //Создаем объект для создания запросов (Statement)
//        Statement stmt = conn.createStatement();
//
//        //Создаем запрос и получаем результат запроса
//        ResultSet rs = stmt.executeQuery("Select * from Persons where firstname="+firstName);
//
//        List<Person> persons= new ArrayList<>();
//
//        while (rs.next()) {
//            persons.add(new Person(rs.getInt("id"), rs.getString("firstName"), rs.getString("lName"), rs.getInt("age"), rs.getString("city")));
//        }
//        rs.close();
//        stmt.close();
//
//        return persons;
//    }
//
//    public static List<Person> searchAllByLastName(String lName) throws SQLException{
//
//        conect();
//
//        //Создаем объект для создания запросов (Statement)
//        Statement stmt = conn.createStatement();
//
//        //Создаем запрос и получаем результат запроса
//        ResultSet rs = stmt.executeQuery("Select * from Persons where lName="+lName);
//
//        List<Person> persons= new ArrayList<>();
//
//        while (rs.next()) {
//            persons.add(new Person(rs.getInt("id"), rs.getString("firstName"), rs.getString("lName"), rs.getInt("age"), rs.getString("city")));
//        }
//        rs.close();
//        stmt.close();
//
//        return persons;
//    }
//
//    public static List<Person> searchAllByAge(int age) throws SQLException{
//
//        conect();
//
//        //Создаем объект для создания запросов (Statement)
//        Statement stmt = conn.createStatement();
//
//        //Создаем запрос и получаем результат запроса
//        ResultSet rs = stmt.executeQuery("Select * from Persons where age="+age);
//
//        List<Person> persons= new ArrayList<>();
//
//        while (rs.next()) {
//            persons.add(new Person(rs.getInt("id"), rs.getString("firstName"), rs.getString("lName"), rs.getInt("age"), rs.getString("city")));
//        }
//        rs.close();
//        stmt.close();
//
//        return persons;
//    }
//
//    public static List<Person> searchAllByCity(String city) throws SQLException{
//
//        conect();
//
//        //Создаем объект для создания запросов (Statement)
//        Statement stmt = conn.createStatement();
//
//        //Создаем запрос и получаем результат запроса
//        ResultSet rs = stmt.executeQuery("Select * from Persons where city="+city);
//
//        List<Person> persons= new ArrayList<>();
//
//        while (rs.next()) {
//            persons.add(new Person(rs.getInt("id"), rs.getString("firstName"), rs.getString("lName"), rs.getInt("age"), rs.getString("city")));
//        }
//        rs.close();
//        stmt.close();
//
//        return persons;
//    }



}

