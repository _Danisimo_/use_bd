package util.remoteDB;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import model.Person;
import model.PersonDAO;
import model.PersonDB;
import util.DBUtil;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostgreDB implements PersonDAO, DBUtil {

//    TABLE Persons
//            (
//             id int PRIMARY KEY NOT NULL,
//             firstName varchar(45),
//             lname varchar(45),
//             age int,
//             city varchar(45)
//)

    //  Database credentials
    protected static final String DB_URL = "jdbc:postgresql://ec2-3-248-4-172.eu-west-1.compute.amazonaws.com:5432/d284cstrro5a7c";
    protected static final String USER = "uboyekdhlpznah";
    protected static final String PASS = "87980139d5440e8e1a14fad6ab0ea6e7b680612a58f79d7c084aa1664a9b4158";
    protected static Connection conn = null;


    public static void dbConnect() {
        //Подключаем драйвер postgre
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
            return;
        }
        //Подключаемся к базе
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
            return;
        }
    }

    public static void dbDisconnect() throws SQLException {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    //DB Execute Query Operation
    public static ResultSet dbExecuteQuery(String queryStmt) throws SQLException, ClassNotFoundException {
        //Declare statement, resultSet and CachedRowSet as null
        Statement stmt = null;
        ResultSet resultSet = null;
        CachedRowSet crs = null;

        try {
            //Connect to DB
            dbConnect();
            System.out.println("Select statement: " + queryStmt + "\n");

            //Create statement
            stmt = conn.createStatement();

            //Execute select (query) operation
            resultSet = stmt.executeQuery(queryStmt);

            //CachedRowSet Implementation
            //In order to prevent "java.sql.SQLRecoverableException: Closed Connection: next" error
            //We are using CachedRowSet
            //crs = new CachedRowSet();
            RowSetFactory aFactory = RowSetProvider.newFactory();
            crs = aFactory.createCachedRowSet();

            crs.populate(resultSet);
        } catch (SQLException e) {
            System.out.println("Problem occurred at executeQuery operation : " + e);
            throw e;
        } finally {
            if (resultSet != null) {
                //Close resultSet
                resultSet.close();
            }
            if (stmt != null) {
                //Close Statement
                stmt.close();
            }
            //Close connection
            dbDisconnect();
        }
        //Return CachedRowSet
        return crs;
    }

    //DB Execute Update (For Update/Insert/Delete) Operation
    public static void dbExecuteUpdate(String sqlStmt) throws SQLException, ClassNotFoundException {
        //Declare statement as null
        Statement stmt = null;
        try {
            //Connect to DB (Establish Oracle Connection)
            dbConnect();
            //Create Statement
            stmt = conn.createStatement();
            //Run executeUpdate operation with given sql statement
            stmt.executeUpdate(sqlStmt);
        } catch (SQLException e) {
            System.out.println("Problem occurred at executeUpdate operation : " + e);
            throw e;
        } finally {
            if (stmt != null) {
                //Close statement
                stmt.close();
            }
            //Close connection
            dbDisconnect();
        }
    }


    //Пробросить все команды через интерфейс и переопределить в классах DB

    //CRUD
    public static void create(String id, String firstName, String lname, String age, String city) throws SQLException, ClassNotFoundException {

        //Declare Creare operation
        String createStmt = "INSERT INTO Persons \n" +
                "VALUES(" + id + ", '" + firstName + "', '" + lname + "', " + age + ", '" + city + "')";

        //Execute Create operation
        try {
            dbExecuteUpdate(createStmt);
        } catch (SQLException | ClassNotFoundException e) {
            System.out.print("Error occurred while UPDATE Operation: " + e);
            throw e;
        }
    }

    public static ObservableList<PersonDB> read() throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM persons ORDER BY id";

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsPers = dbExecuteQuery(selectStmt);

            //Send ResultSet to the getEmployeeList method and get employee object
            ObservableList<PersonDB> personsList = getPersonsList(rsPers);

            //Return employee object
            return personsList;
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("SQL select operation has been failed: " + e);
            //Return exception
            throw e;
        }
    }

    //sup_Read_Function
    private static ObservableList<PersonDB> getPersonsList(ResultSet rs) throws SQLException {
        //Declare a observable List which comprises of Persons objects
        ObservableList<PersonDB> personsList = FXCollections.observableArrayList();

        while (rs.next()) {
            PersonDB person = new PersonDB();
            person.setId(rs.getInt(1));
            person.setFirstName(rs.getString(2));
            person.setLname(rs.getString(3));
            person.setAge(rs.getInt(4));
            person.setCity(rs.getString(5));
            //Add employee to the ObservableList
            personsList.add(person);
        }
        //return empList (ObservableList of Employees)
        return personsList;
    }

    public static void update(String id, String firstName, String lname, String age, String city) throws SQLException, ClassNotFoundException {
        //Declare a UPDATE statement
        String updateStmt = "UPDATE persons\nSet";

        if (!firstName.isEmpty()) {
            updateStmt += " firstName = '" + firstName + "',\n";
        }
        if (!lname.isEmpty()) {
            updateStmt += " lname = '" + lname + "',\n";
        }
        if (!age.isEmpty()) {
            updateStmt += " age = '" + age + "',\n";
        }
        if (!city.isEmpty()) {
            updateStmt += " city = '" + city + "'\n";
        }
        if (updateStmt.endsWith(",")) {
            updateStmt = updateStmt.substring(0, updateStmt.length() - 1);
        }
        updateStmt += "WHERE id = " + id;

        //Execute UPDATE operation
        try {
            dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while UPDATE Operation: " + e);
            throw e;
        }
    }

    public static void delete(String id) throws SQLException, ClassNotFoundException {
        //Declare a DELETE statement
        String updateStmt = "DELETE FROM persons Where id =" + id + "";

        //Execute UPDATE operation
        try {
            dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }

    //Helpers

    //Delete_Block
    public static void deleteAll() throws SQLException, ClassNotFoundException {

        //Declare a DELETE statement
        String updateStmt = "DELETE FROM persons";

        //Execute UPDATE operation
        try {
            dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }

    public static void deleteAllByFirstName(String firstName) throws SQLException, ClassNotFoundException {

        //Declare a DELETE statement
        String updateStmt = "DELETE FROM persons Where firstName='" + firstName + "'";

        //Execute UPDATE operation
        try {
            dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }

    public static void deleteAllByLastName(String lName) throws SQLException, ClassNotFoundException {
        //Declare a DELETE statement
        String updateStmt = "DELETE FROM persons Where lName='" + lName + "'";

        //Execute UPDATE operation
        try {
            dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }

    public static void deleteAllByAge(String age) throws SQLException, ClassNotFoundException {

        //Declare a DELETE statement
        String updateStmt = "DELETE FROM persons Where age=" + age;

        //Execute UPDATE operation
        try {
            dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }

    public static void deleteAllByCity(String city) throws SQLException, ClassNotFoundException {

        //Declare a DELETE statement
        String updateStmt = "DELETE FROM persons Where city='" + city + "'";

        //Execute UPDATE operation
        try {
            dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }


    //Search_Block
    public static PersonDB searchByID(String id) throws SQLException, ClassNotFoundException {

        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM persons WHERE id=" + id;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsPers = dbExecuteQuery(selectStmt);

            //Send ResultSet to the getPersonsFromResultSet method and get person object
            PersonDB person = getPersonsFromResultSet(rsPers);

            //Return employee object
            return person;
        } catch (SQLException e) {
            System.out.println("While searching an employee with " + id + " id, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }

    //Use ResultSet from DB as parameter and set Employee Object's attributes and return person object.
    private static PersonDB getPersonsFromResultSet(ResultSet rs) throws SQLException {
        PersonDB person = null;
        if (rs.next()) {
            person = new PersonDB();
            person.setId(rs.getInt("id"));
            person.setFirstName(rs.getString("firstname"));
            person.setLname(rs.getString("lname"));
            person.setAge(rs.getInt("age"));
            person.setCity(rs.getString("city"));

        }
        return person;
    }

    public static ObservableList<PersonDB> searchByFirstName(String firstName) throws SQLException, ClassNotFoundException {
        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM persons Where firstname= '" + firstName + "'";

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsPersons = dbExecuteQuery(selectStmt);

            //Send ResultSet to the getPerrsonsList method and get employee object
            ObservableList<PersonDB> personList = getPersonsList(rsPersons);

            //Return employee object
            return personList;
        } catch (SQLException e) {
            System.out.println("SQL select operation has been failed: " + e);
            //Return exception
            throw e;
        }
    }

    public static ObservableList<PersonDB> searchAllByLastName(String lName) throws SQLException, ClassNotFoundException {

        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM persons Where lName= '" + lName + "'";

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsPersons = dbExecuteQuery(selectStmt);

            //Send ResultSet to the getPerrsonsList method and get employee object
            ObservableList<PersonDB> personList = getPersonsList(rsPersons);

            //Return employee object
            return personList;
        } catch (SQLException e) {
            System.out.println("SQL select operation has been failed: " + e);
            //Return exception
            throw e;
        }
    }

    public static ObservableList<PersonDB> searchAllByAge(String age) throws SQLException, ClassNotFoundException {

        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM persons Where age= " + age;

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsPersons = dbExecuteQuery(selectStmt);

            //Send ResultSet to the getPerrsonsList method and get employee object
            ObservableList<PersonDB> personList = getPersonsList(rsPersons);

            //Return employee object
            return personList;
        } catch (SQLException e) {
            System.out.println("SQL select operation has been failed: " + e);
            //Return exception
            throw e;
        }
    }

    public static ObservableList<PersonDB> searchAllByCity(String city) throws SQLException, ClassNotFoundException {

        //Declare a SELECT statement
        String selectStmt = "SELECT * FROM persons Where city= '" + city + "'";

        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsPersons = dbExecuteQuery(selectStmt);

            //Send ResultSet to the getPerrsonsList method and get employee object
            ObservableList<PersonDB> personList = getPersonsList(rsPersons);

            //Return employee object
            return personList;
        } catch (SQLException e) {
            System.out.println("SQL select operation has been failed: " + e);
            //Return exception
            throw e;
        }
    }


    //Над имплементациями нужно подумать
    @Override
    public void create() {

    }

    @Override
    public void update(Person p) throws SQLException {
    }

    @Override
    public void delete(Person p) {
    }
//Над имплементациями нужно подумать


}

