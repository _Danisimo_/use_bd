package util.remoteDB;
//
//import com.datastax.oss.driver.api.core.cql.Row;
//import com.datastax.oss.driver.api.core.metadata.Metadata;
//import javafx.util.Builder;
//import org.neo4j.driver.Session;

import javafx.collections.ObservableList;
import model.Person;
import model.PersonDAO;
import model.PersonDB;

import java.sql.SQLException;

public class CassandraDB implements PersonDAO {

    public static ObservableList<PersonDB> read() throws SQLException, ClassNotFoundException {
        return null;
    }

    @Override
    public void create() {

    }

    @Override
    public void update(Person p) throws SQLException {

    }

    @Override
    public void delete(Person p) {

    }

//    public void connect(String node) {
//        cluster = Cluster.builder()
//                .addContactPoint(node)
//                .withCredentials("yourusername", "yourpassword")
//                .build();
//        Metadata metadata = cluster.getMetadata();
//        System.out.printf("Connected to cluster: %s\n",
//                metadata.getClusterName());
//        for (Host host : metadata.getAllHosts()) {
//            System.out.printf("Datatacenter: %s; Host: %s; Rack: %s\n",
//                    host.getDatacenter(), host.getAddress(), host.getRack());
//        }
//        session = cluster.connect();
//    }
//
//
//    //Создание ColumnFamily (aka table)
//    // based on the above keyspace, we would change the cluster and session as follows:
//    Cluster cluster = Cluster.builder()
//            .addContactPoints(serverIP)
//            .build();
//    Session session = cluster.connect("exampkeyspace");
//
//    String cqlStatement = "CREATE TABLE users (" +
//            " username varchar PRIMARY KEY," +
//            " password varchar " +
//            ");";
//
//session.execute(cqlStatement);
//
//
//    // Создание ключевого пространства
//
//    String cqlStatement = "CREATE KEYSPACE exampkeyspace WITH " +
//            "replication = {'class':'SimpleStrategy','replication_factor':1}";
//
//session.execute(cqlStatement);
//
//
//    //Create/Update/Delete
//
//// for all three it works the same way (as a note the 'system' keyspace cant
//// be modified by users so below im using a keyspace name 'exampkeyspace' and
//// a table (or columnfamily) called users
//
//    String cqlStatementC = "INSERT INTO exampkeyspace.users (username, password) " +
//            "VALUES ('Serenity', 'fa3dfQefx')";
//
//    String cqlStatementU = "UPDATE exampkeyspace.users " +
//            "SET password = 'zzaEcvAf32hla'," +
//            "WHERE username = 'Serenity';";
//
//    String cqlStatementD = "DELETE FROM exampkeyspace.users " +
//            "WHERE username = 'Serenity';";
//
//session.execute(cqlStatementC); // interchangeable, put any of the statements u wish.
//
//
//    //Read
//
//    String cqlStatement = "SELECT * FROM local";
//for (Row row : session.execute(cqlStatement)) {
//        System.out.println(row.toString());
//    }
//
//
//
//  //  В примерах я буду использовать следующие две переменные.
//
//    String serverIP = "127.0.0.1";
//    String keyspace = "system";
//
//    Cluster cluster = Cluster.builder()
//            .addContactPoints(serverIP)
//            .build();
//
//    Session session = cluster.connect(keyspace);

// you are now connected to the cluster, congrats!




//   // Подключение к Cassandra очень похоже на подключение к другим источникам данных. С Cassandra учетные данные не требуются.
//
//    String cassandraIPAddress = "127.0.0.1";
//    String cassandraKeyspace = "myKeyspace";
//    String username = "foo";
//    String password = "bar";
//
//    com.datastax.driver.core.Cluster cluster = Cluster.builder()
//            .addContactPoint(cassandraIPAddress)
//            .withCredentials(username, password) // If you have setup a username and password for your node.
//            .build();
//
//    com.datastax.driver.core.Session session = cluster.connect(cassandraKeyspace);
//
//    com.datastax.driver.core.Metadata metadata = cluster.getMetadata();
//
//// Output Cassandra connection status
//System.out.println("Connected to Cassandra cluster: " + metadata.getClusterName() + " with Partitioner: " + metadata.getPartitioner());
//
//// Loop through your entire Cluster.
//for (Host host : metadata.getAllHosts()) {
//        System.out.println("Cassandra Host Address: " + host.getAddress() + " | Is Up = " + host.isUp());
//    }

}
