package util.remoteDB;

import javafx.collections.ObservableList;
import model.Person;
import model.PersonDAO;
import model.PersonDB;

import java.sql.SQLException;

public class H2 implements PersonDAO {


    public static ObservableList<PersonDB> read() throws SQLException, ClassNotFoundException {
        return null;
    }


    @Override
    public void create() {

    }

    @Override
    public void update(Person p) throws SQLException {

    }

    @Override
    public void delete(Person p) {

    }
}
