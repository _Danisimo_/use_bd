package util;

import model.Person;

import java.util.ArrayList;

public class Helpers {

    boolean out;

    public void getAll(ArrayList<Person> persons){
        for (int i = 0; i < persons.size(); i++) {
                System.out.print(persons.get(i).toString()+"\n");
        }
    }

    public void getById(ArrayList<Person> persons, int id) {
        out = false;
        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getId() == id) {
                System.out.println(persons.get(i).toString());
                out = true;
                break;
            }
        }
        if(out == false)
            System.out.println("По запросу \""+ id +"\" ничего не найдено!");
    }

    public void getAllBySity(ArrayList<Person> persons, String sity) {

        out = false;

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getCity().equals(sity)) {
                System.out.println(persons.get(i).toString());
                out = true;
            }
        }
        if(out == false)
            System.out.println("По запросу \""+ sity +"\" ничего не найдено!");
    }

    public void getAllByAge(ArrayList<Person> persons, int age) {

        out = false;

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getAge() == age){
                System.out.println(persons.get(i).toString());
                out = true;
            }
        }
        if(out == false)
            System.out.println("По запросу \""+ age +"\" ничего не найдено!");
    }

    public void getAllByFirstName(ArrayList<Person> persons, String firstName) {

        out = false;

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getFirstName().equals(firstName)) {
                System.out.println(persons.get(i).toString());
                out = true;
            }
        }
        if(out == false)
            System.out.println("По запросу \""+ firstName +"\" ничего не найдено!");
    }

    public void getAllByLastName(ArrayList<Person> persons, String lastName) {

        out = false;

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getLname().equals(lastName)) {
                System.out.println(persons.get(i).toString());
                out =true;
            }
        }
        if(out == false)
            System.out.println("По запросу \""+ lastName +"\" ничего не найдено!");
    }

    public Person getPersonById(ArrayList<Person> persons, int id) {

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getId() == id)
                return persons.get(i);
        }
        return null;
    }
    public void deleteById(int id, ArrayList<Person> persons){
        getById(persons,id);
        persons.remove(getPersonById(persons,id));
    }

    public void delete(int id, ArrayList<Person> persons){ // для того чтобы удалать объкт из списка по айди
        getById(persons,id);
        persons.remove(getPersonById(persons,id));
    }

}
