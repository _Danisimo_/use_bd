package util.servBin;

import model.Person;
import util.localDB.ServiceBinaryFile;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ServiceBinaryFileTest {

    @Test
    public void classServiceBin_Available() {

        ServiceBinaryFile serviceBinaryFile;

        serviceBinaryFile = new ServiceBinaryFile();

        assertNotNull(serviceBinaryFile);
    }

    @Test
    public void test_service_saveAllBinAndParse_void() {
        ServiceBinaryFile serviceBinaryFile = new ServiceBinaryFile();
        serviceBinaryFile.deleteBin();

        ArrayList<Person> expectedPersons = new ArrayList<>();
        expectedPersons.add(new Person(1,"Artemiy","Brook",20,"Kiev"));
        expectedPersons.add(new Person(1,"Vasia","Cherii",19,"Kiev"));

        serviceBinaryFile.saveAllBin(expectedPersons);

        ArrayList<Person> actualPersons = serviceBinaryFile.ParserBin();
        assertEquals(expectedPersons.toString(), actualPersons.toString());
        serviceBinaryFile.deleteBin();
    }

    @Test
    public void test_service_deleteBin_void() {

        ServiceBinaryFile serviceBinaryFile = new ServiceBinaryFile();
        serviceBinaryFile.deleteBin();

        String str ="";
        try(FileInputStream fin=new FileInputStream("person.bin"))
        {
            int i=-1;
            while((i=fin.read())!=-1){

                str+=(char)i;
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        assertEquals("",str);
    }

}
