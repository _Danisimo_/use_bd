package util.servXML;

import model.Person;
import util.localDB.ServiceXML;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ServiceXmlTest {

    private String xmlFile = "persons.xml";
    private Person expectedPersons = new Person(2105, "Вадим", "Горишний", 30, "Ирпень");


    @Before
    public void createTestFile() throws IOException {

        File file = new File(xmlFile);
        file.delete();

        FileWriter writer = new FileWriter(xmlFile, true);

        writer.write("<Person>"
                + "\n<id>" + 2105 + "</id>"
                + "\n<firstname>" + "Вадим" + "</firstname>"
                + "\n<lname>" + "Горишний" + "</lname>"
                + "\n<age>" + 30 + "</age>"
                + "\n<city>" + "Ирпень" + "</city>"
                + "\n</Person>");

        writer.flush();
        writer.close();
    }

    @Test
    public void classAvailable_ok(){

        ServiceXML serviceXML;

        serviceXML = new ServiceXML();

        assertNotNull(serviceXML);
    }

    @Test
    public void parserJson_TestFile_ObjectPerson() {

        ServiceXML serviceXML = new ServiceXML();

        ArrayList<Person> actualArrayList = serviceXML.parserXML();
        Person actualPerson = actualArrayList.get(0);

        assertNotNull(actualPerson);
        assertEquals(expectedPersons.toString(), actualPerson.toString());
    }

    @Test
    public void deleteXML_TestFile_EmptyFile() {

        ServiceXML serviceXML = new ServiceXML();
        String expected = "";
        String actual = "";

        serviceXML.deleteXML();

        try (FileInputStream fin = new FileInputStream("persons.xml")) {
            int i = -1;
            while ((i = fin.read()) != -1) {
                actual += (char) i;
            }

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        assertEquals(expected, actual);
    }


    @After
    public void garbageMan() {
        File file = new File(xmlFile);
        file.delete();
    }
}
