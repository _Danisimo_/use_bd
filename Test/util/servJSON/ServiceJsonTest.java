package util.servJSON;

import model.Person;
import util.localDB.ServiceJSON;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class ServiceJsonTest {

    private String jsonFile = "person.json";
    private Person expectedPersons = new Person(2105, "Вадим", "Горишний", 30, "Ирпень");


    @Before
    public void createTestFile() throws IOException {

        File file = new File(jsonFile);
        file.delete();

        FileWriter writer = new FileWriter(jsonFile, true);

        writer.write("[{"
                + "\"id\" : \"" + 2105 + "\", "
                + "\"firstname\" : \"" + "Вадим" + "\", "
                + "\"lname\" : \"" + "Горишний" + "\", "
                + "\"age\" : \"" + 30 + "\", "
                + "\"city\" : \"" + "Ирпень" + "\"}]");

        writer.flush();
        writer.close();
    }

    @Test
    public void classServiceJSON_Available() {

        ServiceJSON serviceJSON;

        serviceJSON = new ServiceJSON();

        assertNotNull(serviceJSON);
    }

    @Test
    public void parserJson_TestFile_ObjectPerson() {

        ServiceJSON serviceJSON = new ServiceJSON();

        ArrayList<Person> actualArrayList = serviceJSON.parserJson();
        Person actualPerson = actualArrayList.get(0);

        assertNotNull(actualPerson);
        assertEquals(expectedPersons.toString(), actualPerson.toString());
    }

    @Test
    public void deleteJSON_TestFile_EmptyFile() {

        ServiceJSON serviceJSON = new ServiceJSON();
        String expected = "";
        String actual = "";

        serviceJSON.deleteJSON();

        try (FileInputStream fin = new FileInputStream("person.json")) {
            int i = -1;
            while ((i = fin.read()) != -1) {
                actual += (char) i;
            }

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        assertEquals(expected, actual);
    }


    @After
    public void garbageMan() {
        File file = new File(jsonFile);
        file.delete();
    }
}
