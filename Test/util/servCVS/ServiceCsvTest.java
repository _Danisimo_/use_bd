package util.servCVS;

import model.Person;
import org.junit.Test;
import util.localDB.ServiceCSV;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;

public class ServiceCsvTest {

    @Test
    public void test_sevice_csvSaveAllAndParse(){

        ServiceCSV serviceCSV = new ServiceCSV();
        serviceCSV.deleteCSV();

        ArrayList<Person> expectedPersons = new ArrayList<>();
        expectedPersons.add(new Person(1,"Danil","Danil",19,"Lviv"));
        expectedPersons.add(new Person(1,"Maksim","Kapusta",20,"Kiev"));

        serviceCSV.cswSaveAll(expectedPersons);

        ArrayList<Person> actualPersons = serviceCSV.parserCSV();
        assertEquals(expectedPersons.toString(),actualPersons.toString());
        serviceCSV.deleteCSV();
    }

    
    @Test
    public void test_sevice_deleteCSV(){

        ServiceCSV serviceCSV = new ServiceCSV();
        serviceCSV.deleteCSV();

        String str = "";
        try(FileInputStream fin = new FileInputStream("person.csv"))
        {
            int i=-1;
            while((i=fin.read())!=-1){
                str+=(char)i;
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        assertEquals("",str);
    }

}
